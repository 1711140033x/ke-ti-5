class TopController < ApplicationController
   def main
    if session[:login_uid]
      render 'main'
    else
      render 'login'
    end
   end
   def login
   if User.authenticate(params[:uid],params[:pass])
   session[:login] = params[:uid]
   redirect_to top_main_path
   else
   render"error"
   end
   def logout
    session.delete(:login_uid)
    redirect_to top_main_path
   end
   end
end
