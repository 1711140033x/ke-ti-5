class User < ApplicationRecord
    has_many :tweets
    has_many :likes
    has_many :like_tweets, source:tweet, through: :likes
end
